#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "Kocherga";
const char* password = "Wittgenstein";
const char* mqtt_server = "192.168.88.145";

#define MQTT_ID           "idk"
#define MQTT_TEST_TOPIC   "idk/testValue"


WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  int retries = 100;
  while (WiFi.status() != WL_CONNECTED && retries) {
      delay(100);
      retries--;
  }

  if (!retries) {
    ESP.reset();
  }
}

void setup_mqtt() {
  int retries = 50;
  bool connected;
  do {
    connected = client.connect(MQTT_ID);
  if (!connected) delay(100);
    retries--;
  } while (retries && !connected);

  if (!retries) {
    ESP.reset();
  }
}

void setup() {
  setup_wifi();
  setup_mqtt();
}

void loop() {
  client.publish(MQTT_TEST_TOPIC, "A value.");
}
